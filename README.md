VueJS & Firebase
--------------------------

Download your Vue project and start up the test server
1. git clone https://gitlab.com/matwong/firebase-mississauga-coding.git
2. cd firebase-mississauga-coding
3. npm install --save firebase && npm install 
4. npm run build
You should see a new folder dist.


Setting up your Firebase project space and tools
1. Create your Firebase account at https://firebase.google.com/ (top left corner)
2. Go to Firebasebase console from the top right corner next to sign-in
3. Click on "Add project" to create your project
4. Choose a project name, i.e. mw-fb-intro
5. Choose Canada from the country/region pull down
6. Click on Create Project
7. From the project page, click on Database from the left panel
8. Click on Get Start on the Realtime Database panel (right hand side). Just click Enable on the next screen
9. From the left panel (where you select Database from step 7), click on Hosting, then click on Get Start on the next page
10. Take note on the next few pages. Click Continue to proceed to the next step

Install [firebase-tools](https://github.com/firebase/firebase-tools)
From a command prompt / shell:
1. npm install -g firebase-tools
2. type firebase login, and follow the steps (need to sign in from your browser)
3. back to the prompt, type firebase init
    - Select 
        - Database: Deploy Firebase Realtime Database Rules
        - Hosting: Configure and deploy Firebase Hosting sites
        - Storage: Deploy Cloud Storage security rules
        - hit Enter
    - Select the Firebase project, i.e. mw-fb-intro, then hit Enter
    - Hit Enter on the next question (What file should be used for Database Rules? database.rules.json)
    - On the next question "What do you want to use as your public directory? (public)", type dist, then hit Enter
    - Hit Enter on the next few questions, which will use the default setting
4. type firebase deploy
5. Go to the web browser and enter the URL from the output from step4. E.g. https://mw-fb-intro.firebaseapp.com

This web page should show something like [this](https://mw-fb-intro.firebaseapp.com).

Add Firebase Realtime Database to the Vue web page
1. Create secret.json at the root project folder
    - The content of this file is for the API key which should be private, and should be added to the .gitignore 
2. Modifiy src/main.js to include the secret.json file
3. To bypass the database security for now, open database.rules.json and change all false to true

References:
- [Read and Write Data on the Web](https://firebase.google.com/docs/database/)
- [vue-cli Docs](https://github.com/vuejs/vue-cli/blob/dev/docs/README.md)
- [Materialize CSS](https://materializecss.com/)
- [VueJS Official Site](https://vuejs.org/)
- [VueJS By Example from Coursetro](https://coursetro.com/courses/23/Vue-Tutorial-in-2018---Learn-Vue.js-by-Example)
- [Intro to VueJS from VueMastery](https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance)
- [VueJS Examples](https://vuejsexamples.net/)

